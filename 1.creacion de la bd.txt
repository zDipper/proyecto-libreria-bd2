-- MySQL Workbench Forward Engineering

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema libreria_dbV2
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema libreria_dbV2
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `libreria_dbV2` DEFAULT CHARACTER SET latin1 ;
USE `libreria_dbV2` ;

-- -----------------------------------------------------
-- Table `libreria_dbV2`.`pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`pais` (
  `idpais` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idpais`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`autor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`autor` (
  `idAutor` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `pais_idpais` INT(11) NOT NULL,
  PRIMARY KEY (`idAutor`),
  INDEX `fk_autor_pais1_idx` (`pais_idpais` ASC) ,
  CONSTRAINT `fk_autor_pais1`
    FOREIGN KEY (`pais_idpais`)
    REFERENCES `libreria_dbV2`.`pais` (`idpais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`categoria` (
  `idCategoria` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NOT NULL,
  `descripcion` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`persona` (
  `DNI` VARCHAR(8) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `fechaNacimiento` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`DNI`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`cliente` (
  `idCliente` INT(11) NOT NULL AUTO_INCREMENT,
  `persona_DNI` VARCHAR(8) NOT NULL,
  PRIMARY KEY (`idCliente`),
  INDEX `fk_cliente_persona1_idx` (`persona_DNI` ASC) ,
  CONSTRAINT `fk_cliente_persona1`
    FOREIGN KEY (`persona_DNI`)
    REFERENCES `libreria_dbV2`.`persona` (`DNI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`distrito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`distrito` (
  `iddistrito` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`iddistrito`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`editorial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`editorial` (
  `RUC` VARCHAR(11) NOT NULL,
  `nombre` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(50) NULL DEFAULT NULL,
  `telefono` VARCHAR(7) NULL DEFAULT NULL,
  PRIMARY KEY (`RUC`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`empresa` (
  `RUC` VARCHAR(11) NOT NULL,
  `nombre` VARCHAR(30) NOT NULL,
  `direccion` VARCHAR(50) NULL DEFAULT NULL,
  `telefono` VARCHAR(7) NULL DEFAULT NULL,
  `email` VARCHAR(40) NULL DEFAULT NULL,
  PRIMARY KEY (`RUC`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`sucursal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`sucursal` (
  `idsucursal` INT(11) NOT NULL AUTO_INCREMENT,
  `empresa_RUC` VARCHAR(11) NOT NULL,
  `distrito_iddistrito` INT(11) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idsucursal`),
  INDEX `fk_sucursal_empresa1_idx` (`empresa_RUC` ASC) ,
  INDEX `fk_sucursal_distrito1_idx` (`distrito_iddistrito` ASC) ,
  CONSTRAINT `fk_sucursal_distrito1`
    FOREIGN KEY (`distrito_iddistrito`)
    REFERENCES `libreria_dbV2`.`distrito` (`iddistrito`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sucursal_empresa1`
    FOREIGN KEY (`empresa_RUC`)
    REFERENCES `libreria_dbV2`.`empresa` (`RUC`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`empleado` (
  `idEmpleado` VARCHAR(7) NOT NULL,
  `persona_DNI` VARCHAR(8) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `sucursal_idsucursal` INT(11) NOT NULL,
  PRIMARY KEY (`idEmpleado`),
  INDEX `fk_empleado_sucursal1_idx` (`sucursal_idsucursal` ASC) ,
  INDEX `fk_empleado_persona1_idx` (`persona_DNI` ASC) ,
  CONSTRAINT `fk_empleado_sucursal1`
    FOREIGN KEY (`sucursal_idsucursal`)
    REFERENCES `libreria_dbV2`.`sucursal` (`idsucursal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_empleado_persona1`
    FOREIGN KEY (`persona_DNI`)
    REFERENCES `libreria_dbV2`.`persona` (`DNI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`genero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`genero` (
  `idgenero` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idgenero`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`idioma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`idioma` (
  `ididioma` INT(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ididioma`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`tapa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`tapa` (
  `idtapa` INT(11) NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idtapa`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`libro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`libro` (
  `ISBN` VARCHAR(13) NOT NULL,
  `titulo` VARCHAR(40) NOT NULL,
  `editorial_RUC` VARCHAR(11) NOT NULL,
  `stock` INT NOT NULL,
  `precioCompra` DOUBLE NOT NULL,
  `precioVenta` DOUBLE NOT NULL,
  `tapa_idtapa` INT(11) NOT NULL,
  `idioma_ididioma` INT(11) NOT NULL,
  `categoria_idCategoria` INT(11) NOT NULL,
  `numeroPag` INT(11) NULL DEFAULT NULL,
  `descripcion` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`ISBN`),
  INDEX `fk_libro_categoria1_idx` (`categoria_idCategoria` ASC) ,
  INDEX `fk_libro_tapa1_idx` (`tapa_idtapa` ASC) ,
  INDEX `fk_libro_editorial1_idx` (`editorial_RUC` ASC) ,
  INDEX `fk_libro_idioma1_idx` (`idioma_ididioma` ASC) ,
  CONSTRAINT `fk_libro_categoria1`
    FOREIGN KEY (`categoria_idCategoria`)
    REFERENCES `libreria_dbV2`.`categoria` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_editorial1`
    FOREIGN KEY (`editorial_RUC`)
    REFERENCES `libreria_dbV2`.`editorial` (`RUC`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_tapa1`
    FOREIGN KEY (`tapa_idtapa`)
    REFERENCES `libreria_dbV2`.`tapa` (`idtapa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_idioma1`
    FOREIGN KEY (`idioma_ididioma`)
    REFERENCES `libreria_dbV2`.`idioma` (`ididioma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`venta` (
  `idventa` INT(11) NOT NULL AUTO_INCREMENT,
  `empleado_idEmpleado` VARCHAR(7) NOT NULL,
  `cliente_idCliente` INT(11) NOT NULL,
  `fecha` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`idventa`),
  INDEX `fk_venta_empleado1_idx` (`empleado_idEmpleado` ASC) ,
  INDEX `fk_venta_cliente1_idx` (`cliente_idCliente` ASC) ,
  CONSTRAINT `fk_venta_cliente1`
    FOREIGN KEY (`cliente_idCliente`)
    REFERENCES `libreria_dbV2`.`cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_empleado1`
    FOREIGN KEY (`empleado_idEmpleado`)
    REFERENCES `libreria_dbV2`.`empleado` (`idEmpleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`autor_has_libro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`autor_has_libro` (
  `autor_idAutor` INT(11) NOT NULL,
  `libro_ISBN` VARCHAR(13) NOT NULL,
  PRIMARY KEY (`autor_idAutor`, `libro_ISBN`),
  INDEX `fk_autor_has_libro_libro1_idx` (`libro_ISBN` ASC) ,
  INDEX `fk_autor_has_libro_autor1_idx` (`autor_idAutor` ASC) ,
  CONSTRAINT `fk_autor_has_libro_autor1`
    FOREIGN KEY (`autor_idAutor`)
    REFERENCES `libreria_dbV2`.`autor` (`idAutor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_autor_has_libro_libro1`
    FOREIGN KEY (`libro_ISBN`)
    REFERENCES `libreria_dbV2`.`libro` (`ISBN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`venta_has_libro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`venta_has_libro` (
  `venta_idventa` INT(11) NOT NULL,
  `libro_ISBN` VARCHAR(13) NOT NULL,
  `cantidad` INT NOT NULL,
  `precioUnitario` DOUBLE NULL,
  PRIMARY KEY (`venta_idventa`, `libro_ISBN`),
  INDEX `fk_venta_has_libro_libro1_idx` (`libro_ISBN` ASC) ,
  INDEX `fk_venta_has_libro_venta1_idx` (`venta_idventa` ASC) ,
  CONSTRAINT `fk_venta_has_libro_venta1`
    FOREIGN KEY (`venta_idventa`)
    REFERENCES `libreria_dbV2`.`venta` (`idventa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_has_libro_libro1`
    FOREIGN KEY (`libro_ISBN`)
    REFERENCES `libreria_dbV2`.`libro` (`ISBN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`libro_has_genero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`libro_has_genero` (
  `libro_ISBN` VARCHAR(13) NOT NULL,
  `genero_idgenero` INT(11) NOT NULL,
  PRIMARY KEY (`libro_ISBN`, `genero_idgenero`),
  INDEX `fk_libro_has_genero_genero1_idx` (`genero_idgenero` ASC) ,
  INDEX `fk_libro_has_genero_libro1_idx` (`libro_ISBN` ASC) ,
  CONSTRAINT `fk_libro_has_genero_libro1`
    FOREIGN KEY (`libro_ISBN`)
    REFERENCES `libreria_dbV2`.`libro` (`ISBN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_has_genero_genero1`
    FOREIGN KEY (`genero_idgenero`)
    REFERENCES `libreria_dbV2`.`genero` (`idgenero`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`proveedor` (
  `ruc` VARCHAR(11) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NULL,
  `telefono` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `distrito_iddistrito` INT(11) NOT NULL,
  PRIMARY KEY (`ruc`),
  INDEX `fk_proveedor_distrito1_idx` (`distrito_iddistrito` ASC) ,
  CONSTRAINT `fk_proveedor_distrito1`
    FOREIGN KEY (`distrito_iddistrito`)
    REFERENCES `libreria_dbV2`.`distrito` (`iddistrito`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`compra` (
  `idcompra` INT NOT NULL,
  `proveedor_ruc` VARCHAR(11) NOT NULL,
  `sucursal_idsucursal` INT(11) NOT NULL,
  `fecha` DATE NULL,
  PRIMARY KEY (`idcompra`),
  INDEX `fk_compra_proveedor1_idx` (`proveedor_ruc` ASC) ,
  INDEX `fk_compra_sucursal1_idx` (`sucursal_idsucursal` ASC) ,
  CONSTRAINT `fk_compra_proveedor1`
    FOREIGN KEY (`proveedor_ruc`)
    REFERENCES `libreria_dbV2`.`proveedor` (`ruc`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_sucursal1`
    FOREIGN KEY (`sucursal_idsucursal`)
    REFERENCES `libreria_dbV2`.`sucursal` (`idsucursal`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `libreria_dbV2`.`compra_has_libro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `libreria_dbV2`.`compra_has_libro` (
  `compra_idcompra` INT NOT NULL,
  `libro_ISBN` VARCHAR(13) NOT NULL,
  `cantidad` INT NOT NULL,
  `precioUnitario` DOUBLE NULL,
  PRIMARY KEY (`compra_idcompra`, `libro_ISBN`),
  INDEX `fk_compra_has_libro_libro1_idx` (`libro_ISBN` ASC) ,
  INDEX `fk_compra_has_libro_compra1_idx` (`compra_idcompra` ASC) ,
  CONSTRAINT `fk_compra_has_libro_compra1`
    FOREIGN KEY (`compra_idcompra`)
    REFERENCES `libreria_dbV2`.`compra` (`idcompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_has_libro_libro1`
    FOREIGN KEY (`libro_ISBN`)
    REFERENCES `libreria_dbV2`.`libro` (`ISBN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

